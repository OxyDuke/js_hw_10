document.addEventListener("DOMContentLoaded", function () {
    const tabs = document.querySelectorAll(".tabs-title");
    const tabContents = document.querySelectorAll(".tabs-content li");

    tabs.forEach(tab => {
        tab.addEventListener("click", () => {
           const tabId = tab.getAttribute("data-tab");

            tabs.forEach(t => t.classList.remove("active"));
            tabContents.forEach(content => content.classList.remove("active"));

            tab.classList.add("active");
            document.getElementById(tabId).classList.add("active");
        });
    });
});